package com.idfy.capture.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.os.Looper;
import android.provider.Settings;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.idfy.core.components.Components;
import com.idfy.capture.R;
import com.idfy.core.components.ModelComponents.ModelButton;
import com.idfy.core.components.ModelComponents.ModelImageView;
import com.idfy.core.components.ModelComponents.ModelLinearLayout;
import com.idfy.core.components.ModelComponents.ModelTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PermissionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PermissionFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "PAGE_NAME";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private JSONObject jsonData;
    private BaseFragment baseFragment;
    private LinearLayout parentLayout;
    private Components components;
    private LinearLayout attriLinearLayout;
    private boolean isLocationPermissionMandatory;
    private boolean isCameraPermissionMandatory;
    private boolean isMicPermissionMandatory;
    private FusedLocationProviderClient mFusedLocationClient;
    private int PERMISSION_ID = 44;


    public PermissionFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static PermissionFragment newInstance(String param1) {
        PermissionFragment fragment = new PermissionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        components = new Components(getActivity()); // this should not be here, access from parent fragment
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        baseFragment = ((BaseFragment) PermissionFragment.this.getParentFragment());
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            JSONObject payloadObject = null;
            try {
                payloadObject = new JSONObject();
                payloadObject.put("page", mParam1);
                payloadObject.put("payload", new JSONArray());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            baseFragment.fetchPageConfig(mParam1, payloadObject);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_permission, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        parentLayout = view.findViewById(R.id.permission_parent_layout);
        parentLayout.setBackgroundColor(Color.parseColor(baseFragment.getSecondaryMainColor()));
    }

    public void setJsonPayload(JSONObject jsonData) {
        try {
            JSONObject dataObject = jsonData.getJSONObject("data");
            JSONObject configObject = dataObject.getJSONObject("config");
            String headerTitle = configObject.optString("title");
            JSONArray captureArray = configObject.getJSONArray("capture");
            if (captureArray.length() > 0) {
                ModelLinearLayout modelLinearLayout = new ModelLinearLayout();
                modelLinearLayout.setHeight(0);
                modelLinearLayout.setWeight(1.8f);
                attriLinearLayout = components.getLinearLayout(modelLinearLayout);
                requireActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        baseFragment.setToolbarTitle(headerTitle);
                        parentLayout.addView(attriLinearLayout);

                    }
                });
            }
            for (int i = 0; i < captureArray.length(); i++) {
                JSONObject captureObject = captureArray.getJSONObject(i);
                String attr = captureObject.getString("attr");
                JSONObject attributeObject = captureObject.getJSONObject("template");
                boolean mandatory = attributeObject.getBoolean("mandatory");
                String label = attributeObject.getString("label");
                if (attr.equals("location")) {
                    isLocationPermissionMandatory = mandatory;
                    renderPermissionUi(R.drawable.ic_location, label, attriLinearLayout);
                } else if (attr.equals("camera")) {
                    isCameraPermissionMandatory = mandatory;
                    renderPermissionUi(R.drawable.ic_camera, label, attriLinearLayout);

                } else if (attr.equals("microphone")) {
                    isMicPermissionMandatory = mandatory;
                    renderPermissionUi(R.drawable.ic_mic, label, attriLinearLayout);
                }
            }
            renderAllowPermissionBtn();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void renderPermissionUi(int resource, String label, LinearLayout attriLinearLayout) {
        ModelLinearLayout horizontalModelLinearLayout = new ModelLinearLayout();
        horizontalModelLinearLayout.setMarginTop(requireActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
        horizontalModelLinearLayout.setMarginBottom(requireActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
        horizontalModelLinearLayout.setMarginRight(requireActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
        horizontalModelLinearLayout.setMarginLeft(requireActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
        horizontalModelLinearLayout.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        horizontalModelLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        horizontalModelLinearLayout.setGravity(Gravity.CENTER_VERTICAL);
        LinearLayout horizontalLinearLayout = components.getLinearLayout(horizontalModelLinearLayout);

        ModelImageView modelImageView = new ModelImageView();
        modelImageView.setWidth(requireActivity().getResources().getDimensionPixelSize(R.dimen._25sdp));
        modelImageView.setHeight(requireActivity().getResources().getDimensionPixelSize(R.dimen._25sdp));
        modelImageView.setResource(resource);
        ImageView attributeImageView = components.getImageView(modelImageView);

        ModelTextView modelTextView = new ModelTextView();
        modelTextView.setText(label);
        modelTextView.setTextColor("#FFFFFF");
        modelTextView.setMarginLeft(requireActivity().getResources().getDimensionPixelSize(R.dimen._5sdp));
        modelTextView.setSize(requireActivity().getResources().getDimensionPixelSize(R.dimen._5ssp));
        TextView labelTextView = components.getTextView(modelTextView);
        requireActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                attriLinearLayout.addView(horizontalLinearLayout);
                horizontalLinearLayout.addView(attributeImageView);
                horizontalLinearLayout.addView(labelTextView);


            }
        });


    }

    private void renderAllowPermissionBtn() {
        ModelLinearLayout modelLinearLayout = new ModelLinearLayout();
        modelLinearLayout.setHeight(0);
        modelLinearLayout.setWeight(0.2f);
        LinearLayout btnLinearLayout = components.getLinearLayout(modelLinearLayout);

        ModelButton modelButton = new ModelButton();
        modelButton.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        modelButton.setMarginLeft(requireActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
        modelButton.setMarginRight(requireActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
        modelButton.setText("Allow Permission");
        modelButton.setTextColor(baseFragment.getPrimaryContrastColor());
        modelButton.setBackgroundColor(baseFragment.getPrimaryMainColor());
        Button allowButton = components.getButton(modelButton);
        requireActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentLayout.addView(btnLinearLayout);
                btnLinearLayout.addView(allowButton);
                allowButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        askForPermission();
                    }
                });


            }
        });
    }

    private void askForPermission() {
        Dexter.withContext(requireActivity())
                .withPermissions(
                        (isCameraPermissionMandatory) ? Manifest.permission.CAMERA : "",
                        Manifest.permission.MODIFY_AUDIO_SETTINGS,
                        (isMicPermissionMandatory) ? Manifest.permission.RECORD_AUDIO : "",
                        (isLocationPermissionMandatory) ? Manifest.permission.ACCESS_COARSE_LOCATION : "",
                        (isLocationPermissionMandatory) ? Manifest.permission.ACCESS_FINE_LOCATION : "",
                        (isLocationPermissionMandatory) ? Manifest.permission.ACCESS_BACKGROUND_LOCATION : "",
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.WAKE_LOCK)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            String nextPage = baseFragment.onNextPage();
                            if (nextPage.equals("capture")) {
                                CaptureFragment captureFragment = CaptureFragment.newInstance(nextPage);
                                baseFragment.loadFragment(captureFragment, "CaptureFragment");
                            }

                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    private void getLastLocation() {
        // check if permissions are given
        if (checkPermissions()) {

            // check if location is enabled
            if (isLocationEnabled()) {

                // getting last
                // location from
                // FusedLocationClient
                // object
                if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mFusedLocationClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        Location location = task.getResult();
                        if (location == null) {
                            requestNewLocationData();
                        } else {
                            // latitudeTextView.setText(location.getLatitude() + "");
                            // longitTextView.setText(location.getLongitude() + "");
                        }
                    }
                });
            } else {
                Toast.makeText(getActivity(), "Please turn on" + " your location...", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
           askForPermission();
        }
    }

    @SuppressLint("MissingPermission")
    private void requestNewLocationData() {

        // Initializing LocationRequest
        // object with appropriate methods
        LocationRequest mLocationRequest =  LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(5);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);

        // setting LocationRequest
        // on FusedLocationClient
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
    }

    private LocationCallback mLocationCallback = new LocationCallback() {

        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
           // latitudeTextView.setText("Latitude: " + mLastLocation.getLatitude() + "");
            //longitTextView.setText("Longitude: " + mLastLocation.getLongitude() + "");
        }
    };

    // method to check for permissions
    private boolean checkPermissions() {
        return ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        // If we want background location
        // on Android 10.0 and higher,
        // use:
        // ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED
    }


    // method to check
    // if location is enabled
    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) requireActivity().getSystemService(requireActivity().LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }



}