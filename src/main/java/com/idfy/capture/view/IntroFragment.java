package com.idfy.capture.view;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.idfy.core.components.Components;
import com.idfy.capture.R;
import com.idfy.core.components.ModelComponents.ModelButton;
import com.idfy.core.components.ModelComponents.ModelLinearLayout;
import com.idfy.core.components.ModelComponents.ModelTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link IntroFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class IntroFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "JSON_DATA";
    private String JSON_DATA;
    private LinearLayout introParentLayout;
    private Components components;
    private BaseFragment baseFragment;

    public IntroFragment() {
        // Required empty public constructor
    }


    public static IntroFragment newInstance(JSONObject param1) {
        IntroFragment fragment = new IntroFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1.toString());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseFragment = ((BaseFragment)IntroFragment.this.getParentFragment());
        if (baseFragment != null){
//            JSONObject pageVisited = new JSONObject();
//            try {
//                pageVisited.put("visited_page",pageName);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            baseFragment.sendPageVisitedLog(pageVisited);
        }
        if (getArguments() != null) {
            JSON_DATA = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_intro, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        components = new Components(requireActivity());
        introParentLayout = view.findViewById(R.id.introParentLayout);
        introParentLayout.setBackgroundColor(Color.parseColor(baseFragment.getSecondaryMainColor()));
        //getPageData(mParam1);
        try {
            JSONObject jsonData = new JSONObject(JSON_DATA);
            parsePage(jsonData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    private void parsePage(JSONObject envelope) {
        try {
            JSONObject dataObject = envelope.getJSONObject("data");
            JSONObject configObject = dataObject.getJSONObject("config");
            parseMasterIntroductionPage(configObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseMasterIntroductionPage(JSONObject configObject){
        String introTitle = configObject.optString("title");
        ModelLinearLayout introLinearLayout = new ModelLinearLayout();
        introLinearLayout.setHeight(0);
        introLinearLayout.setWeight(1.8f);
        introLinearLayout.setOrientation(LinearLayout.VERTICAL);
        introLinearLayout.setMarginTop(requireActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
        introLinearLayout.setMarginBottom(requireActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
        introLinearLayout.setMarginRight(requireActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
        introLinearLayout.setMarginLeft(requireActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
        LinearLayout introLayout = components.getLinearLayout(introLinearLayout);
        ModelTextView subModelTextView = new ModelTextView();
        subModelTextView.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        subModelTextView.setTextColor(baseFragment.getSecondaryContrastColor());
        //  subModelTextView.setG
        subModelTextView.setSize(requireActivity().getResources().getDimensionPixelSize(R.dimen._5ssp));
        subModelTextView.setMarginTop(requireActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
        TextView subTextView = components.getTextView(subModelTextView);
        subTextView.setText(Html.fromHtml(configObject.optString("text"), Html.FROM_HTML_MODE_LEGACY));
        if (!introTitle.isEmpty()) {
            ModelTextView titleModelTextView = new ModelTextView();
            titleModelTextView.setText(introTitle);
            titleModelTextView.setTextColor(baseFragment.getSecondaryContrastColor());
            titleModelTextView.setSize(requireActivity().getResources().getDimensionPixelSize(R.dimen._6ssp));
            TextView titleTextView = components.getTextView(titleModelTextView);
            requireActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    introParentLayout.addView(introLayout);
                    introLayout.addView(titleTextView);
                    introLayout.addView(subTextView);
                }
            });
        }else {
            requireActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    introParentLayout.addView(introLayout);
                    introLayout.addView(subTextView);
                }
            });
        }
        ModelLinearLayout proceedModel = new ModelLinearLayout();
        proceedModel.setHeight(0);
        proceedModel.setWeight(0.2f);
        proceedModel.setGravity(Gravity.CENTER);
        LinearLayout proceedLinearLayout = components.getLinearLayout(proceedModel);
        ModelButton prModelButton = new ModelButton();
        prModelButton.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        prModelButton.setBackgroundColor(baseFragment.getPrimaryMainColor());
        prModelButton.setTextColor(baseFragment.getPrimaryContrastColor());
        prModelButton.setMarginLeft(requireActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
        prModelButton.setMarginRight(requireActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
        prModelButton.setText("Proceed");
        Button proceedButton = components.getButton(prModelButton);
        requireActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                introParentLayout.addView(proceedLinearLayout);
                proceedLinearLayout.addView(proceedButton);
                proceedButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        introParentLayout.removeAllViews();
//                        int currentPage = baseFragment.getPageIndex();
//                        currentPage++;
//                        baseFragment.setPageIndex(currentPage);
                       // getPageData(baseFragment.getPageName());
                        String pageName = baseFragment.onNextPage();
                        if (pageName.equals("consent")) {
                            JSONObject payloadObject = null;
                            try {
                                payloadObject = new JSONObject();
                                payloadObject.put("page",pageName);
                                payloadObject.put("payload",new JSONArray());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            baseFragment.fetchPageConfig(pageName,payloadObject);
                        }else if (pageName.equals("permissions")){
                            PermissionFragment permissionFragment = PermissionFragment.newInstance(pageName);
                            baseFragment.loadFragment(permissionFragment,"PermissionFrag");
                        }
                    }
                });

            }
        });

    }

    public void setPayloadData(JSONObject object) {
        parsePage(object);
    }
}