package com.idfy.capture.view;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.idfy.core.DALCapture;
import com.idfy.core.components.Components;
import com.idfy.capture.R;
import com.idfy.core.components.ModelComponents.ModelButton;
import com.idfy.core.components.ModelComponents.ModelCardView;
import com.idfy.core.components.ModelComponents.ModelImageView;
import com.idfy.core.components.ModelComponents.ModelLinearLayout;
import com.idfy.core.components.ModelComponents.ModelTextInputLayout;
import com.idfy.core.components.ModelComponents.ModelTextView;
import com.idfy.core.logger.LogLevel;
import com.idfy.core.logger.ModalLogDetails;
import com.idfy.core.model.PageSequence;
import com.idfy.core.model.ThemeConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;


public class CaptureFragment extends Fragment {

    private static final String ARG_PARAM1 = "PAGE_NAME";
    private static final String ARG_PARAM2 = "param2";
    private String pageName;
    private String mParam2;
    private String captureId;
    private String session_id;
    private String requestId;
    private String token = "9rq2axvfjOeq"; // new token: MZ-0y9fw3EB-
    private String sessionToken = "";
    private DALCapture dalCapture = null;
    private LinearLayout parentLinearLayout;
    private Components components = null;
    private LinearLayout cardLinearLayout = null;
    private CardView cardView;
    private LinearLayout headerLayout;
    private LinearLayout contentLayout;
    private LinearLayout footerLayout;
    private JSONObject themeObject;
    private TextView footer_title;
    private BaseFragment baseFragment;
    private LinearLayout captureParentLinearLayout;
    private View view;
    private LinearLayout mainLayout;
    private Button btnInitiate;
    private ThemeConfig themeConfig;
    private long loggerStartTime;


    public CaptureFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static CaptureFragment newInstance(String page) {
        CaptureFragment fragment = new CaptureFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, page);
        // args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dalCapture = DALCapture.getInstance();
        themeConfig = dalCapture.getCaptureTheme();
        baseFragment = ((BaseFragment) CaptureFragment.this.getParentFragment());
        components = new Components(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_capture, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        loggerStartTime = System.currentTimeMillis();
        initView(view);
    }

    private void initView(View view) {
        baseFragment.setCurrentFragmentName("CaptureFrag");
        captureParentLinearLayout = view.findViewById(R.id.capture_parent_layout);
        mainLayout = view.findViewById(R.id.capture_main_layout);
        btnInitiate = view.findViewById(R.id.btnInitiate);
        btnInitiate.setTextColor(Color.parseColor(themeConfig.getPrimaryContrastColor()));
        btnInitiate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (baseFragment != null) {
                    baseFragment.hideHeader();
                }
                JSONObject initiateObject = new JSONObject();
                JSONObject artifactsObject = new JSONObject();
                JSONObject taskObject = new JSONObject();
                try {
                    initiateObject.put("artifacts", artifactsObject);
                    initiateObject.put("tasks", taskObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dalCapture.sendEvent("session:initiate", initiateObject);
                mainLayout.removeAllViews();
                ViewStub viewStub = components.getViewStub();
                mainLayout.addView(viewStub);
                viewStub.setLayoutResource(R.layout.layout_thank_you);
                View view = viewStub.inflate();
                RelativeLayout thankRelativeLayout = view.findViewById(R.id.thank_you_main_layout);
                thankRelativeLayout.setBackgroundColor(Color.parseColor(themeConfig.getSecondaryMainColor()));

            }
        });

        mainLayout.setBackgroundColor(Color.parseColor(themeConfig.getSecondaryMainColor()));
        if (baseFragment != null) {
            baseFragment.setToolbarTitle("Capture Details");
            baseFragment.setToolbarImage(R.drawable.idfy_main_logo);
            JSONObject pageVisited = new JSONObject();
            try {
                pageVisited.put("visited_page", pageName);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            baseFragment.sendPageVisitedLog(pageVisited);
            if (getArguments() != null) {
                pageName = getArguments().getString(ARG_PARAM1);
                TimeZone tz = TimeZone.getTimeZone("UTC");
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                df.setTimeZone(tz);
                String nowAsISO = df.format(new Date());
                JSONObject payloadObject = null;
                try {
                    payloadObject = new JSONObject();
                    payloadObject.put("page", pageName);
                    JSONArray payloadArray = new JSONArray();
                    JSONObject object = new JSONObject();
                    object.put("attr", "location");
                    JSONObject data = new JSONObject();
                    data.put("latitude", Double.parseDouble(dalCapture.getLatitude()));
                    data.put("longitude", Double.parseDouble(dalCapture.getLongitude()));
                    data.put("altitude", Double.parseDouble(dalCapture.getAltitude()));
                    data.put("accuracy", Double.parseDouble(dalCapture.getAccuracy()));
                    data.put("altitudeAccuracy", null);
                    data.put("heading", null);
                    data.put("speed", dalCapture.getSpeed());
                    data.put("timestamp", nowAsISO);
                    data.put("source", "GPS");
                    object.put("data", data);
                    payloadArray.put(0, object);
                    payloadObject.put("payload", payloadArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                baseFragment.fetchPageConfig(pageName, payloadObject);
            }
        }
    }

    public void setCapturePayload(JSONObject jsonObject) {
        try {
            JSONObject dataObject = jsonObject.getJSONObject("data");
            JSONObject configObject = dataObject.getJSONObject("config");
            if (configObject.has("capture")) {
                JSONObject taskObject = configObject.getJSONObject("tasks");
                if (taskObject.has("vkyc.assisted_vkyc")) {
                    JSONObject vKycObject = taskObject.getJSONObject("vkyc.assisted_vkyc");
                    requestId = vKycObject.optString("request_uid");
                    dalCapture.setRequestId(requestId);
                }

                ModalLogDetails logDetails = new ModalLogDetails();
                logDetails.service_category = "Capture";
                logDetails.service = "RoutePage";
                //  logDetails.event_type = dalCapture.getCurrentPage().getPage();
                logDetails.event_name = "";
                logDetails.component = "IndvCaptureComponent";
                logDetails.event_source = "setCurrentPage";
                baseFragment.getLoggerWrapper().log(LogLevel.Info, logDetails, new HashMap<>());

                parseVKycResponse(configObject);
                baseFragment.getLoggerWrapper().logPageRender(loggerStartTime, "Capture", new HashMap<>());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseVKycResponse(JSONObject configObject) {
        try {
            captureParentLinearLayout.removeAllViewsInLayout();
            JSONArray captureArray = configObject.getJSONArray("capture");
            for (int i = 0; i < captureArray.length(); i++) {
                JSONArray jsonArray = captureArray.getJSONArray(i);
                for (int j = 0; j < jsonArray.length(); j++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(j);

                    int templateId = jsonObject.optInt("template_id");

                    JSONObject templateObject = configObject.getJSONObject("templates");
                    JSONObject idObject = templateObject.getJSONObject(String.valueOf(templateId));


                    String type = idObject.optString("type");
                    String title = idObject.optString("title");
                    String detail = idObject.optString("detail");
                    if (type.equals("card")) {
                        renderCardUi(title, detail, j, themeConfig.getSecondaryMainColor(), themeConfig.getSecondaryContrastColor());
                        JSONArray taskArray = jsonObject.getJSONArray("tasks");
                        int template_id = 0;
                        for (int k = 0; k < taskArray.length(); k++) {
                            JSONObject taskUiObject = taskArray.getJSONObject(k);
                            template_id = taskUiObject.optInt("template_id");
                            Log.d("===TEMP", String.valueOf(template_id));
                            JSONObject taskIdObject = templateObject.getJSONObject(String.valueOf(template_id));
                            String taskType = taskIdObject.optString("type");
                            String taskKey = taskIdObject.optString("task_key");
                            String taskLabel = taskIdObject.optString("label");
                            Log.d("===TASK", taskType);
                            if (taskType.equals("button")) {
                                renderCaptureButton(taskLabel, taskKey, themeConfig.getPrimaryMainColor(), themeConfig.getPrimaryContrastColor(), j);
                            } else if (taskType.equals("text")) {
                                renderEditTextUi(taskLabel, j);
                            } else if (taskType.equals("image")) {
                                renderImageCaptureUi(j, themeConfig.getSecondaryContrastColor());
                            } else if (taskType.equals("group")) {
                                renderImageCaptureUi(j, themeConfig.getSecondaryContrastColor());
                            } else if (taskType.equals("form")) {
                                if (taskUiObject.has("tasks")) {
                                    JSONArray innerTaskArray = taskUiObject.getJSONArray("tasks");
                                    for (int l = 0; l < innerTaskArray.length(); l++) {
                                        JSONObject innerTaskObject = innerTaskArray.getJSONObject(l);
                                        template_id = innerTaskObject.optInt("template_id");
                                        JSONObject taskOb = templateObject.getJSONObject(String.valueOf(template_id));
                                        String task_t = taskOb.optString("type");
                                        String task_k = taskOb.optString("task_key");
                                        String task_l = taskOb.optString("label");
                                        if (task_t.equals("anchor")) {
                                            renderAnchor(j, themeConfig.getSecondaryContrastColor(), task_l);
                                        } else if (task_t.equals("text")) {
                                            renderEditTextUi(task_l, j);

                                        }

                                    }
                                }
                                renderCaptureButton(taskLabel, taskKey, themeConfig.getPrimaryMainColor(),
                                        themeConfig.getPrimaryContrastColor(), j);
                            }


                        }
                    }

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void renderCardUi(String title, String subLabel, int id, String mainColor, String textColor) {
        Log.d("===DATA", title + " " + subLabel + " " + id);
        ModelCardView modelCardView = new ModelCardView();
        modelCardView.setBackgroundColor(mainColor);
        modelCardView.setElevation(25f);
        modelCardView.setMarginLeft(requireActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
        modelCardView.setMarginRight(requireActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
        modelCardView.setMarginTop(requireActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
        modelCardView.setMarginBottom(requireActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
        modelCardView.setRadius(15);
        CardView cardView = components.getCardView(modelCardView);

        ModelLinearLayout modallinear = new ModelLinearLayout();
        modallinear.setId(id);
        modallinear.setOrientation(LinearLayout.VERTICAL);
        modallinear.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        modallinear.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        modallinear.setMarginLeft(requireActivity().getResources().getDimensionPixelSize(R.dimen._15sdp));
        modallinear.setMarginRight(requireActivity().getResources().getDimensionPixelSize(R.dimen._15sdp));
        modallinear.setMarginTop(requireActivity().getResources().getDimensionPixelSize(R.dimen._15sdp));
        modallinear.setMarginBottom(requireActivity().getResources().getDimensionPixelSize(R.dimen._15sdp));
        LinearLayout cardLinearLayout = components.getLinearLayout(modallinear);

        ModelTextView modelTextView = new ModelTextView();
        modelTextView.setText(title);
        modelTextView.setTextColor(textColor);
        modelTextView.setSize(requireActivity().getResources().getDimensionPixelSize(R.dimen._6ssp));
        TextView titleView = components.getTextView(modelTextView);

        ModelTextView subTitleModel = new ModelTextView();
        subTitleModel.setText((!subLabel.equals("null")) ? subLabel : "");
        subTitleModel.setTextColor(textColor);
        subTitleModel.setSize(requireActivity().getResources().getDimensionPixelSize(R.dimen._4ssp));
        TextView subTextView = components.getTextView(subTitleModel);
        requireActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // captureParentLinearLayout.setBackgroundColor(Color.parseColor(mainColor));
                captureParentLinearLayout.addView(cardView);
                cardView.addView(cardLinearLayout);
                cardLinearLayout.addView(titleView);
                cardLinearLayout.addView(subTextView);

            }
        });

    }

    private void renderAnchor(int id, String textColor, String anchorText) {
        ModelTextView anchorModelTextView = new ModelTextView();
        //anchorModelTextView.setText(Html.fromHtml("Click here to generate xml",Html.FROM_HTML_MODE_LEGACY));
        anchorModelTextView.setTextColor(textColor);
        anchorModelTextView.setMarginTop(requireActivity().getResources()
                .getDimensionPixelSize(R.dimen._10sdp));
        anchorModelTextView.setSize(requireActivity().getResources().getDimensionPixelSize(R.dimen._5ssp));
        TextView anchorTextView = components.getTextView(anchorModelTextView);
        anchorTextView.setClickable(true);
        anchorTextView.setMovementMethod(LinkMovementMethod.getInstance());
        String text = "<a href=>" + anchorText + "</a>";
        anchorTextView.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY));
        requireActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout linearLayout = view.findViewById(id);
                linearLayout.addView(anchorTextView);
            }
        });
    }

    private void renderImageCaptureUi(int id, String textColor) {
        ModelLinearLayout captureModel = new ModelLinearLayout();
        captureModel.setGravity(Gravity.CENTER_HORIZONTAL);
        captureModel.setMarginTop(requireActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
        captureModel.setPaddingTop(requireActivity().getResources().getDimensionPixelSize(R.dimen._5sdp));
        captureModel.setPaddingBottom(requireActivity().getResources().getDimensionPixelSize(R.dimen._5sdp));
        captureModel.setPaddingLeft(requireActivity().getResources().getDimensionPixelSize(R.dimen._5sdp));
        captureModel.setPaddingRight(requireActivity().getResources().getDimensionPixelSize(R.dimen._5sdp));
        captureModel.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        captureModel.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout captureLinearLayout = components.getLinearLayout(captureModel);
        captureLinearLayout.setBackground(requireActivity().getResources().getDrawable(R.drawable.blue_border));

        ModelImageView captureModelImageView = new ModelImageView();
        captureModelImageView.setWidth(requireActivity().getResources().getDimensionPixelSize(R.dimen._40sdp));
        captureModelImageView.setHeight(requireActivity().getResources().getDimensionPixelSize(R.dimen._40sdp));
        captureModelImageView.setResource(R.drawable.ic_capture);
        ImageView captureImageView = components.getImageView(captureModelImageView);

        ModelTextView captureModelTextView = new ModelTextView();
        captureModelTextView.setMarginTop(requireActivity().getResources().getDimensionPixelSize(R.dimen._5sdp));
        captureModelTextView.setText("Click To Capture");
        captureModelTextView.setTextColor(textColor);
        TextView captureTextView = components.getTextView(captureModelTextView);
        requireActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                captureLinearLayout.addView(captureImageView);
                captureLinearLayout.addView(captureTextView);
                LinearLayout linearLayout = view.findViewById(id);
                linearLayout.addView(captureLinearLayout);

            }
        });

    }

    private void renderEditTextUi(String label, int id) {
        ModelTextInputLayout modelTextInputLayout = new ModelTextInputLayout();
        modelTextInputLayout.setHint(label);
        modelTextInputLayout.setMarginTop(requireActivity().getResources()
                .getDimensionPixelSize(R.dimen._5sdp));
        TextInputLayout textInputLayout = components.getTextInputLayout(modelTextInputLayout);
        requireActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout linearLayout = view.findViewById(id);
                linearLayout.addView(textInputLayout);
            }
        });

    }

    private void renderCaptureButton(String taskLabel, String taskKey, String mainColor, String textColor, int id) {
        requireActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ModelButton modelButton = new ModelButton();
                modelButton.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
                modelButton.setText(taskLabel);
                modelButton.setTextColor(textColor);
                modelButton.setBackgroundColor(mainColor);
                // modelButton.setMarginLeft(20);
                if (dalCapture.isCallCompleted()) {
                    btnInitiate.setEnabled(true);
                    modelButton.setEnable(false);
                    btnInitiate.getBackground().setColorFilter(Color.parseColor(themeConfig.getPrimaryMainColor()), PorterDuff.Mode.MULTIPLY);
                } else {
                    btnInitiate.setEnabled(false);
                    modelButton.setEnable(true);
                    btnInitiate.getBackground().setColorFilter(Color.DKGRAY, PorterDuff.Mode.MULTIPLY);
                }
                modelButton.setMarginTop(requireActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
                Button videoCallButton = components.getButton(modelButton);
                videoCallButton.setTag(taskKey);

                LinearLayout linearLayout = view.findViewById(id);
                linearLayout.addView(videoCallButton);
                videoCallButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (v.getTag().equals("vkyc.assisted_vkyc")) {
                            ModalLogDetails logDetails = new ModalLogDetails();
                            logDetails.service_category = "Capture";
                            logDetails.service = "InitiateAVKYC";
                            logDetails.event_type = "Clicked";
                            logDetails.event_name = "StartVKYCcoupled";
                            logDetails.component = "ButtonComponent";
                            logDetails.event_source = "handleClick";
                            baseFragment.getLoggerWrapper().log(LogLevel.Info, logDetails, new HashMap<>());
                            PageSequence nextPage = dalCapture.onNextPage();
                            if (nextPage.getPage().equals("PAGE_END")) {
                                JSONObject payloadObject = null;
                                try {
                                    payloadObject = new JSONObject();
                                    payloadObject.put("page", taskKey);
                                    JSONArray jsonArray = new JSONArray();
                                    payloadObject.put("payload", jsonArray);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                baseFragment.fetchPageConfig(taskKey,payloadObject);
//                                StateCheckFragment stateCheckFragment = StateCheckFragment.newInstance(taskKey);
//                                baseFragment.loadFragment(stateCheckFragment, "StateCheckFrag");

                            }
                        }
                    }


                });
            }
        });

    }


}