package com.idfy.capture.view;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.idfy.core.DALCapture;
import com.idfy.core.logger.LogLevel;
import com.idfy.core.logger.ModalLogDetails;
import com.idfy.core.model.ThemeConfig;
import com.idfy.videocomponent.VideoComponent;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.idfy.core.components.Components;
import com.idfy.capture.R;
import com.idfy.core.components.ModelComponents.ModelButton;
import com.idfy.core.components.ModelComponents.ModelCheckBox;
import com.idfy.core.components.ModelComponents.ModelLinearLayout;
import com.idfy.core.components.ModelComponents.ModelTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class StateCheckFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "PAGE_NAME";
    private static final String ARG_PARAM2 = "THEME_CONFIG";
    private static final String ARG_PARAM3 = "TOKEN";
    private static final String ARG_PARAM4 = "SESSION_ID";
    private static final String ARG_PARAM5 = "PUBLIC_ID";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private LinearLayout avkycParentContainer;
    private Components uiComponents = null;
    private LinearLayout avkyc_header_layout;
    private LinearLayout avkyc_content_layout;
    private LinearLayout avkyc_footer_layout;
    private LinearLayout contentInnerLinearLayout;
    private TextView avkyc_footer_title;
    private DALCapture dalCapture;
    private BaseFragment baseFragment;
    private Button readyButton;
    private int checkSurrounding = 0;
    private int checkBoxCount = 0;
    private JSONObject networkConfigObject;
    private boolean isLastAttempt = false;
    private boolean isAttemptExhausted = false;
    private ThemeConfig themeConfig;
    private long loggerStartTime;


    public StateCheckFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static StateCheckFragment newInstance(String param1) {
        StateCheckFragment fragment = new StateCheckFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2.toString());
//        args.putString(ARG_PARAM3, token);
//        args.putString(ARG_PARAM4, sessionId);
//        args.putString(ARG_PARAM5, publicId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseFragment = ((BaseFragment) StateCheckFragment.this.getParentFragment());
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
//            themeConfig = getArguments().getString(ARG_PARAM2);
//            token = getArguments().getString(ARG_PARAM3);
//            sessionId = getArguments().getString(ARG_PARAM4);
//            publicId = getArguments().getString(ARG_PARAM5);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_avkyc, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        baseFragment.setCurrentFragmentName("StateCheckFrag");
        initView(view);
        loggerStartTime = System.currentTimeMillis();

        // parseThemeConfig();
        if (baseFragment != null) {
            JSONObject payloadObject = null;
            try {
                payloadObject = new JSONObject();
                payloadObject.put("page", mParam1);
                JSONArray jsonArray = new JSONArray();
                payloadObject.put("payload", jsonArray);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            baseFragment.fetchPageConfig(mParam1, payloadObject);
        }
    }

    public void setStateCheckPayLoad(JSONObject object) {

        ModalLogDetails logDetails = new ModalLogDetails();
        logDetails.service_category = "Capture";
        logDetails.service = "RoutePage";
        logDetails.event_type = "vkyc.assisted_vkyc";
        logDetails.event_name = "";
        logDetails.component = "Button";
        logDetails.event_source = "routeToAssistedVKyc";
        baseFragment.getLoggerWrapper().log(LogLevel.Info, logDetails, new HashMap<>());

        parseAVKycDataUi(object);
        baseFragment.getLoggerWrapper().logPageRender(loggerStartTime, "AssistedVideoKYC", new HashMap<>());

    }

    private void initView(View view) {
        dalCapture = DALCapture.getInstance();
        themeConfig = dalCapture.getCaptureTheme();
        uiComponents = new Components(getActivity());
        avkycParentContainer = view.findViewById(R.id.avkyc_parent_linear_layout);
        if (!themeConfig.getSecondaryMainColor().isEmpty()) {
            avkycParentContainer.setBackgroundColor(Color.parseColor(themeConfig.getSecondaryMainColor()));

        }
        avkyc_content_layout = view.findViewById(R.id.avkyc_content_layout);
    }

    JSONObject stateCheckjsonObject;

    private void parseAVKycDataUi(JSONObject jsonObject) {
        stateCheckjsonObject = jsonObject;
        try {

            JSONObject dataObject = jsonObject.getJSONObject("data");
            JSONObject attemptObject = dataObject.getJSONObject("attempts");
            isLastAttempt = attemptObject.getBoolean("last_attempt");
            isAttemptExhausted = attemptObject.getBoolean("attempts_exhausted");
            JSONObject configObject = dataObject.getJSONObject("config");
            networkConfigObject = configObject.getJSONObject("network_check_config");
            JSONObject videoOverlayObject = configObject.getJSONObject("video_overlay");
            dalCapture.setVideoOverlayEnabled(videoOverlayObject.getBoolean("enabled"));
            JSONObject introObject = configObject.getJSONObject("introduction");
            String title = introObject.optString("title");
            // renderToolBar(introObject.optString("title"),"#FFFFFF","#000000");
            JSONObject prerequisiteCard = introObject.getJSONObject("prerequisite_card");
            ModelLinearLayout modelLinearLayout = new ModelLinearLayout();
            modelLinearLayout.setOrientation(LinearLayout.VERTICAL);
            modelLinearLayout.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
            modelLinearLayout.setWeight(1.9f);
            contentInnerLinearLayout = uiComponents.getLinearLayout(modelLinearLayout);
            requireActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    baseFragment.setToolbarTitle(title);
                    baseFragment.setToolbarImage(R.drawable.ic_back);
                    avkyc_content_layout.addView(contentInnerLinearLayout);
                }
            });
            renderTitle(prerequisiteCard.optString("title"));
            boolean displayBox = prerequisiteCard.optBoolean("box");
            boolean displayCheck = prerequisiteCard.optBoolean("display_as_checklist");
            JSONArray prerequisiteItems = prerequisiteCard.getJSONArray("prerequisite_items");
            for (int i = 0; i < prerequisiteItems.length(); i++) {
                if (displayCheck) {
                    checkBoxCount++;
                    ModelCheckBox modelCheckBox = new ModelCheckBox();
                    modelCheckBox.setText(prerequisiteItems.optString(i));
                    modelCheckBox.setPaddingLeft(getActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
                    modelCheckBox.setPaddingRight(getActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
                    modelCheckBox.setPaddingTop(getActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
                    modelCheckBox.setPaddingBottom(getActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
                    modelCheckBox.setMarginLeft(getActivity().getResources().getDimensionPixelSize(R.dimen._5sdp));
                    modelCheckBox.setMarginRight(getActivity().getResources().getDimensionPixelSize(R.dimen._5sdp));
                    modelCheckBox.setMarginTop(getActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
                    modelCheckBox.setTextColor(themeConfig.getSecondaryContrastColor());
                    CheckBox checkBox = uiComponents.getCheckBox(modelCheckBox);
                    checkBox.setId(i);
                    checkBox.setButtonTintList(requireActivity().getColorStateList(R.color.white));
                    if (displayBox) {
                        checkBox.setBackground(getActivity().getDrawable(R.drawable.checkbox_selector));
                    }
                    requireActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked) {
                                        checkBox.setButtonTintList(requireActivity().getColorStateList(R.color.purple_200));
                                        checkSurrounding++;
                                    } else {
                                        checkBox.setButtonTintList(requireActivity().getColorStateList(R.color.white));
                                        checkSurrounding--;
                                    }
                                    readyButton.getBackground().setColorFilter((checkSurrounding == checkBoxCount) ? Color.parseColor(themeConfig.getPrimaryMainColor()) :
                                            Color.DKGRAY, PorterDuff.Mode.MULTIPLY);
                                    readyButton.setEnabled(checkSurrounding == checkBoxCount);
                                    readyButton.setClickable(checkSurrounding == checkBoxCount);
                                    readyButton.setText("I'M READY (" + checkSurrounding + "/" + checkBoxCount + ")");

                                }
                            });
                            contentInnerLinearLayout.addView(checkBox);

                        }
                    });

                } else {
                    ModelTextView modelTextView = new ModelTextView();
                    modelTextView.setText(prerequisiteItems.optString(i));
                    modelTextView.setPaddingLeft(getActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
                    modelTextView.setPaddingRight(getActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
                    modelTextView.setPaddingTop(getActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
                    modelTextView.setPaddingBottom(getActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
                    modelTextView.setMarginLeft(getActivity().getResources().getDimensionPixelSize(R.dimen._5sdp));
                    modelTextView.setMarginRight(getActivity().getResources().getDimensionPixelSize(R.dimen._5sdp));
                    modelTextView.setMarginTop(getActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
                    modelTextView.setTextColor(themeConfig.getSecondaryContrastColor());
                    TextView textView = uiComponents.getTextView(modelTextView);
                    if (displayBox) {
                        textView.setBackground(AppCompatResources.getDrawable(requireActivity(), R.drawable.checkbox_selector));
                    }
                    requireActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            contentInnerLinearLayout.addView(textView);

                        }
                    });
                }
            }
            ModelLinearLayout linearLayout = new ModelLinearLayout();
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
            linearLayout.setWeight(0.1f);
            LinearLayout buttonLayout = uiComponents.getLinearLayout(linearLayout);
            ModelButton readyModelButton = new ModelButton();
            readyModelButton.setTextColor(themeConfig.getPrimaryContrastColor());
            readyModelButton.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
            readyModelButton.setMarginLeft(requireActivity().getResources().getDimensionPixelSize(R.dimen._15sdp));
            readyModelButton.setMarginRight(requireActivity().getResources().getDimensionPixelSize(R.dimen._15sdp));
            if (displayCheck) {
                readyModelButton.setEnable(false);
                readyModelButton.setText("I'M READY (" + checkSurrounding + "/" + checkBoxCount + ")");
            } else {
                readyModelButton.setBackgroundColor(themeConfig.getPrimaryMainColor());
                readyModelButton.setEnable(true);
                readyModelButton.setText("I'M READY");
            }
            requireActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    readyButton = uiComponents.getButton(readyModelButton);
                    if (displayCheck) {
                        readyButton.setEnabled(false);
                        readyButton.setClickable(false);
                    }
                    avkyc_content_layout.addView(buttonLayout);
                    buttonLayout.addView(readyButton);
                    readyButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            ModalLogDetails logDetails = new ModalLogDetails();
                            logDetails.service_category = "Capture";
                            logDetails.service = "ConnectVSGateway";
                            logDetails.event_type = "Clicked";
                            logDetails.event_name = "Ready";
                            logDetails.component = "StartVideoComponent";
                            logDetails.event_source = "startClicked";
                            logDetails.reference_id = dalCapture.getRequestId();
                            logDetails.reference_type = "AV.TaskID";

                            Map<String, Object> meta = new HashMap<>();
                            meta.put("request_uid", dalCapture.getRequestId());

                            baseFragment.getLoggerWrapper().log(LogLevel.Info, logDetails, meta);


                            avkyc_content_layout.removeAllViews();
                            ViewStub viewStub = uiComponents.getViewStub();
                            avkyc_content_layout.addView(viewStub);
                            viewStub.setLayoutResource(R.layout.layout_check_connection);
                            View view = viewStub.inflate();
                            themeForConnectionPage(view);
                        }
                    });

                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void renderTitle(String title) {
        ModelTextView titleModelTextView = new ModelTextView();
        titleModelTextView.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        titleModelTextView.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        titleModelTextView.setSize(getActivity().getResources().getDimensionPixelSize(R.dimen._6ssp));
        titleModelTextView.setMarginLeft(getActivity().getResources().getDimensionPixelSize(R.dimen._5sdp));
        titleModelTextView.setMarginTop(getActivity().getResources().getDimensionPixelSize(R.dimen._10sdp));
        titleModelTextView.setText(getString(R.string.read_for_call));
        titleModelTextView.setTextColor(themeConfig.getSecondaryContrastColor());
        TextView titleTextView = uiComponents.getTextView(titleModelTextView);

        ModelTextView textViewModel = new ModelTextView();
        textViewModel.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        textViewModel.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        textViewModel.setSize(getActivity().getResources().getDimensionPixelSize(R.dimen._4ssp));
        textViewModel.setMarginLeft(getActivity().getResources().getDimensionPixelSize(R.dimen._5sdp));
        textViewModel.setMarginTop(getActivity().getResources().getDimensionPixelSize(R.dimen._5sdp));
        textViewModel.setText(title);
        textViewModel.setTextColor(themeConfig.getSecondaryContrastColor());
        TextView textView = uiComponents.getTextView(textViewModel);
        requireActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                contentInnerLinearLayout.addView(titleTextView);
                contentInnerLinearLayout.addView(textView);
            }
        });

    }

    private void themeForConnectionPage(View view) {
        ConstraintLayout parentConstraintLayout = view.findViewById(R.id.parent_check_connection);
        parentConstraintLayout.setBackgroundColor(Color.parseColor(themeConfig.getSecondaryMainColor()));
        TextView info1 = view.findViewById(R.id.info1);
        info1.setTextColor(Color.parseColor(themeConfig.getSecondaryContrastColor()));
        TextView info2 = view.findViewById(R.id.info2);
        info2.setTextColor(Color.parseColor(themeConfig.getSecondaryContrastColor()));
        TextView info3 = view.findViewById(R.id.info3);
        info3.setTextColor(Color.parseColor(themeConfig.getSecondaryContrastColor()));
        TextView info4 = view.findViewById(R.id.info4);
        info4.setTextColor(Color.parseColor(themeConfig.getSecondaryContrastColor()));
        TextView info5 = view.findViewById(R.id.info5);
        info5.setTextColor(Color.parseColor(themeConfig.getSecondaryContrastColor()));
        CardView cardView = view.findViewById(R.id.cardView);
        cardView.setBackgroundColor(Color.parseColor(themeConfig.getSecondaryMainColor()));
        Button btnVideoCall = view.findViewById(R.id.btnVideoCall);
        btnVideoCall.getBackground().setColorFilter(Color.parseColor(themeConfig.getPrimaryMainColor()), PorterDuff.Mode.MULTIPLY);
        btnVideoCall.setTextColor(Color.parseColor(themeConfig.getPrimaryContrastColor()));
        btnVideoCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // temperorily commented for testing purpose
                /*if (isAttemptExhausted || isLastAttempt) {

                    ModalLogDetails logDetails = new ModalLogDetails();
                    logDetails.service_category = "Capture";
                    logDetails.service = "VideoCallState";
                    logDetails.event_type = "Clicked";
                    logDetails.event_name = isLastAttempt ? "LastAttempt" : "AttemptsExhausted";
                    logDetails.component = "StartVideoComponent";
                    logDetails.event_source = "startClicked";
                    logDetails.reference_id = dalCapture.getRequestId();
                    logDetails.reference_type = "AV.TaskID";

                    Map<String, Object> meta = new HashMap<>();
                    meta.put("request_uid", dalCapture.getRequestId());
                    baseFragment.getLoggerWrapper().log(LogLevel.Info, logDetails, meta);


                    showLastAttemptDialog(view);
                } else {
                    askForPermission();
                }*/
                askForPermission();
            }
        });
    }

    private void startVideoCapture() {

        ModalLogDetails logDetails = new ModalLogDetails();
        logDetails.service_category = "Capture";
        logDetails.service = "ConnectVSGateway";
        logDetails.event_type = "Clicked";
        logDetails.event_name = "StartVideoCall";
        logDetails.component = "StartVideoComponent";
        logDetails.event_source = "startClicked";
        logDetails.reference_id = dalCapture.getRequestId();
        logDetails.reference_type = "AV.TaskID";

        Map<String, Object> meta = new HashMap<>();
        meta.put("request_uid", dalCapture.getRequestId());

        baseFragment.getLoggerWrapper().log(LogLevel.Info, logDetails, meta);

//        VideoComponent videoComponent = new VideoComponent();
//        videoComponent.init(networkConfigObject.toString(), baseFragment.getChildFragMan(), R.id.content_layout, "CameraViewFrag",);
    }

    private void askForPermission() {
        Dexter.withContext(requireActivity())
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.MODIFY_AUDIO_SETTINGS,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.WAKE_LOCK)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            //getSessionToken();
                            startVideoCapture();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showLastAttemptDialog(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ViewGroup viewGroup = view.findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.last_attempt_dialog_layout, viewGroup, false);
        LinearLayout lastAttemptLayout = dialogView.findViewById(R.id.lastAttemptLayout);
        lastAttemptLayout.setBackgroundColor(Color.parseColor(themeConfig.getSecondaryMainColor()));
        TextView txtLastAttemptHeader = dialogView.findViewById(R.id.txtLastAttemptHeader);
        if (isAttemptExhausted) {
            txtLastAttemptHeader.setText(getText(R.string.maximum_attempt));
        }
        txtLastAttemptHeader.setTextColor(Color.parseColor(themeConfig.getSecondaryContrastColor()));
        TextView txtLastAttemptHeader2 = dialogView.findViewById(R.id.txtLastAttemptHeader2);
        txtLastAttemptHeader2.setTextColor(Color.parseColor(themeConfig.getSecondaryContrastColor()));
        TextView txtLastAttemptHeader3 = dialogView.findViewById(R.id.txtLastAttemptHeader3);
        txtLastAttemptHeader3.setTextColor(Color.parseColor(themeConfig.getSecondaryContrastColor()));
        builder.setPositiveButton("OKAY", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                if (isAttemptExhausted) {
                    dialog.dismiss();
                    // clear back stack and
                    // show thank you screen
                } else {
                    askForPermission();
                }
            }
        });
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        //alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor(primaryContrastColor));
        //alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.parseColor(secondaryMainColor));
        alertDialog.show();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }



}