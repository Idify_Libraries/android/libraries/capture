package com.idfy.capture.view;

import androidx.fragment.app.FragmentManager;

import org.json.JSONObject;

/**
 * This class do the overall capture process
 * create a instance of this class in your activity
 * with the instance call the {@link #startCapture(String, FragmentManager, int, CallBack)}
 */

public class Capture {

    BaseFragment baseFragment;
    CallBack callBack;

    public Capture(){ }

    /**
     * This Method should be called first to initiate the capture process pass the below params
     * @param token your token
     * @param fragmentManager activity fragmentManager
     * @param containerId layout id where the Ui should be shown
     * @param callBack callback for success
     */

    public void startCapture(String token, FragmentManager fragmentManager, int containerId, CallBack callBack) {

        this.callBack = callBack;
        baseFragment = BaseFragment.newInstance(token);
        fragmentManager.beginTransaction()
                .add(containerId, baseFragment).commit();

        baseFragment.initCallBack(new BaseFragment.CaptureCallBack() {
            @Override
            public void onCaptureSuccess(JSONObject object) {
                callBack.onCaptureSuccess(object);
            }

            @Override
            public void onCaptureFailure(JSONObject object) {
                callBack.onCaptureFailure(object);
            }
        });
    }

    /**
     * Pass this callback to {@link #startCapture(String, FragmentManager, int, CallBack)} Method
     * to get the success callback
     */

    public interface CallBack{
        void onCaptureSuccess(JSONObject object);
        void onCaptureFailure(JSONObject object);
    }

    /**
     * Wrap your activity super.onBackPressed() with this condition
     * eg:
     * if (capture.onBackPress()){
     *        super.onBackPressed();
     *   }
     * @return true/false
     */

    public boolean onBackPress(){
        if (baseFragment.getChildFragmentManager().getBackStackEntryCount() > 1) {
            baseFragment.getChildFragmentManager().popBackStackImmediate();
        } else {
            return true;
        }
        return false;
    }
}
