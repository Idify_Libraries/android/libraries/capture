package com.idfy.capture.view;

import static com.idfy.core.utils.Constants.Status_Approved;
import static com.idfy.core.utils.Constants.Status_Cancelled;
import static com.idfy.core.utils.Constants.Status_Capture_Expired;
import static com.idfy.core.utils.Constants.Status_Capture_Pending;
import static com.idfy.core.utils.Constants.Status_Failed;
import static com.idfy.core.utils.Constants.Status_In_Progress;
import static com.idfy.core.utils.Constants.Status_Initiated;
import static com.idfy.core.utils.Constants.Status_Processed;
import static com.idfy.core.utils.Constants.Status_Recapture_Pending;
import static com.idfy.core.utils.Constants.Status_Rejected;
import static com.idfy.core.utils.Constants.Status_Review;
import static com.idfy.core.utils.Constants.Status_Review_Required;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentResultListener;
import androidx.fragment.app.FragmentTransaction;

import android.os.CountDownTimer;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.idfy.capture.utils.MyUtility;
import com.idfy.core.DALCapture;
import com.idfy.core.api.ApiCallBack;
import com.idfy.core.components.Components;
import com.idfy.capture.R;
import com.idfy.core.components.ModelComponents.ModelImageView;
import com.idfy.core.components.ModelComponents.ModelTextView;
import com.idfy.core.components.ModelComponents.ModelToolBar;
import com.idfy.core.logger.LogLevel;
import com.idfy.core.logger.Logger;
import com.idfy.core.logger.LoggerWrapper;
import com.idfy.core.logger.ModalLogDetails;
import com.idfy.core.model.PageSequence;
import com.idfy.core.model.ThemeConfig;
import com.idfy.videocomponent.CameraViewFragment;
import com.idfy.vkyc.AssistedVideoKyc;
import com.idfy.vkyc.AssistedVideoKycFragment;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BaseFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BaseFragment extends Fragment implements AssistedVideoKyc.CallBack {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "TOKEN";
    
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private LinearLayout headerLinearLayout;
    private LinearLayout contentLinearLayout;
    private LinearLayout footerLinearLayout;
    private DALCapture dalCapture;
    private List<PageSequence> listPageSeq = new ArrayList<>();
    private TextView footer_title;
    private String secondaryContrastColor = "";
    private String secondaryMainColor = "";
    private String primaryMainColor = "";
    private String primaryContrastColor = "";
    private String token = "";
    private String captureId;
    private String session_id;
    private String requestId;
    private String sessionToken = "";
    private Components components = null;
    private int pageIndex = 0;
    private Fragment currentFragment = null;
    private TextView toolbarTitle;
    private ImageView toolbarImage;
    private RelativeLayout rlControl;
    private Button btnRetry;
    private FragmentManager childFragMan;
    private RelativeLayout info_main_layout;
    private LinearLayout parentLayout;
    private TextView txtStatus;
    private TextView txtStatusMessage;
    private FusedLocationProviderClient mFusedLocationClient;
    private static final String FRAGMENT_REQUEST = "CAMERA_VIEW_CALLBACK";
    private CountDownTimer cTimer;
    LoggerWrapper loggerWrapper;
    private long loggerStartTime;

    public BaseFragment() {
    }

    /**
     * This Fragment will initiate the capture process pass the token in the params it will return the instance of {@link BaseFragment}
     * and Load this Fragment in Activity
     *
     * @param param1
     * @return fragment
     */

    public static BaseFragment newInstance(String param1) {
        BaseFragment fragment = new BaseFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            token = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_base, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        initLogger();
        initView(view);
    }

    private void initLogger() {
        loggerWrapper = LoggerWrapper.getInstance();
        String loggerUrl = "https://capture.kyc.idfystaging.com/js-logger/publish";
        loggerUrl = "https://webhook.site/1adc0e3f-c10e-4848-ba0f-fbd63df90db2";

        // Create instance of logger
        loggerWrapper.createLogger(getActivity(), "capture", token, loggerUrl);
        loggerWrapper.createLogger(getActivity(), "pg", token, loggerUrl);
        loggerWrapper.createLogger(getActivity(), "healthCheck", token, loggerUrl);

        // send logs: init logger
        ModalLogDetails logDetails = new ModalLogDetails();
        logDetails.service_category = "Capture";
        logDetails.service = "LogUserAgent";
        logDetails.event_type = "Profile.UA";
        logDetails.event_name = "Android app " + Build.MODEL + " " + Build.PRODUCT;
        logDetails.event_source = "init";
        logDetails.component = "loggerWrapper";

        loggerWrapper.log(LogLevel.Info, logDetails, new HashMap<>());
    }

    private void initView(View view) {
        childFragMan = getChildFragmentManager();
        initFragmentListener();
        components = new Components(getActivity());
        dalCapture = DALCapture.getInstance();
        txtStatus = view.findViewById(R.id.status);
        txtStatusMessage = view.findViewById(R.id.status_message);
        parentLayout = view.findViewById(R.id.parentLayout);
        headerLinearLayout = view.findViewById(R.id.header_layout);
        info_main_layout = view.findViewById(R.id.info_main_layout);
        contentLinearLayout = view.findViewById(R.id.content_layout);
        footerLinearLayout = view.findViewById(R.id.footer_layout);
        footer_title = view.findViewById(R.id.footer_title);
        btnRetry = view.findViewById(R.id.btnRetry);
        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MyUtility.isInternetAvailable(requireActivity())) {
                    if (currentFragment instanceof CaptureFragment) {
                        parentLayout.setVisibility(View.GONE);
                        info_main_layout.setVisibility(View.GONE);
                        dalCapture.disconnectDalSocket();
                        getCaptureStatusDetails(false);
                    } else if (currentFragment instanceof CameraViewFragment) {
                        showNetworkDisconnected(false);
                        clearBackStack();
                        ((CameraViewFragment) currentFragment).networkDisconnectonOnSessionError();
                        PageSequence pageSequence = dalCapture.getFirstPage();
                        loadFragment(CaptureFragment.newInstance(pageSequence.getPage()), "CaptureFrag");
                    }
                } else {
                    Toast.makeText(requireActivity(), getString(R.string.no_internet_available), Toast.LENGTH_SHORT).show();
                }
            }
        });
        askForPermission();


    }

    private void getCaptureStatusDetails(boolean override) {
        // send logs: before apiCaptureStatusDetails
        ModalLogDetails logDetails = new ModalLogDetails();
        logDetails.service_category = "Capture";
        logDetails.service = "TokenAuth";
        logDetails.event_type = "Authenticate";
        logDetails.event_name = "";
        logDetails.event_source = "apiCaptureStatusDetails";
        logDetails.component = "DataService Core";

        loggerWrapper.log(LogLevel.Info, logDetails, new HashMap<>());
        loggerStartTime = System.currentTimeMillis();

        dalCapture.getCaptureStatusDetails(getActivity(), token, override, new ApiCallBack() {
            @Override
            public void onSessionSuccess(JSONObject object) {
                renderCaptureUi(object);
            }

            @Override
            public void intermediateCallBack() {
            }

            @Override
            public void onSessionFailure(String message) {
                if (message.equals(getString(R.string.dont_have_permission))) {

                    // send logs: apiCaptureStatusDetails multiple session override
                    ModalLogDetails logDetails = new ModalLogDetails();
                    logDetails.service_category = "Capture";
                    logDetails.service = "TokenAuth";
                    logDetails.event_type = "";
                    logDetails.event_name = "'MULTIPLE_SESSIONS'";
                    logDetails.event_source = "apiCaptureStatusDetails";
                    logDetails.component = "DataService Core";

                    // add meta data as overrides: true
                    loggerWrapper.log(LogLevel.Info, logDetails, new HashMap<>());
                    multipleSessionDialog();
                }

            }
        });
    }

    private void renderCaptureUi(JSONObject object) {
        try {
            parentLayout.setVisibility(View.VISIBLE);
            info_main_layout.setVisibility(View.GONE);
            String headerTitle = object.getString("title");
            ThemeConfig themeConfig = dalCapture.getCaptureTheme();
            if (themeConfig.isDisplayFooter()) {
                footer_title.setVisibility(View.VISIBLE);
            }
            parentLayout.setBackgroundColor(Color.parseColor(themeConfig.getSecondaryMainColor()));
            footerLinearLayout.setBackgroundColor(Color.parseColor(themeConfig.getSecondaryMainColor()));

            // send logs: after apiCaptureStatusDetails load time
            ModalLogDetails logDetails = new ModalLogDetails();
            logDetails.service_category = "Capture";
            logDetails.service = "TokenAuth";
            logDetails.event_name = dalCapture.getTatSince(loggerStartTime);
            logDetails.event_type = "Auth.TAT";
            logDetails.event_source = "apiCaptureStatusDetails";
            logDetails.component = "DataService Core";

            // set defaults
            ModalLogDetails logDefaults = new ModalLogDetails();
            logDefaults.reference_id = dalCapture.getCaptureId();
            logDefaults.reference_type = "CaptureID";
            loggerWrapper.setDefault("capture", logDefaults);


            // set default meta
            Map<String, Object> metaDefaults = new HashMap<>();
            metaDefaults.put("capture_id", dalCapture.getCaptureId());
            metaDefaults.put("profile_status", dalCapture.getStatus());
            metaDefaults.put("session_id", dalCapture.getSessionId());
            loggerWrapper.setMetaDefaults("capture", metaDefaults);

            loggerWrapper.log(LogLevel.Info, logDetails, new HashMap<>());

            // render toolbar
            renderToolBar(themeConfig.getHeaderBackgroundColor(), themeConfig.getHeaderTextColor(), themeConfig.getLogo()
                    , headerTitle);


            // send logs: after apiCaptureStatusDetails Authenticated
            logDetails = new ModalLogDetails();
            logDetails.service_category = "Capture";
            logDetails.service = "TokenAuth";
            logDetails.event_name = "";
            logDetails.event_type = "Authenticated";
            logDetails.event_source = "apiCaptureStatusDetails";
            logDetails.component = "DataService Core";
            loggerWrapper.log(LogLevel.Info, logDetails, new HashMap<>());

            processCapture();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void processCapture() {
        switch (dalCapture.getStatus()) {
            case Status_Capture_Pending:
                getPageData();
                break;
            case Status_Review:
            case Status_Initiated:
            case Status_In_Progress:
            case Status_Review_Required:
                showCaptureStatus(dalCapture.getStatus(), getResources().getColor(R.color.orange));
                break;
            case Status_Approved:
            case Status_Processed:
                showCaptureStatus(dalCapture.getStatus(), getResources().getColor(R.color.green));
                break;
            case Status_Capture_Expired:
            case Status_Recapture_Pending:
            case Status_Failed:
            case Status_Cancelled:
            case Status_Rejected:
                showCaptureStatus(dalCapture.getStatus(), getResources().getColor(R.color.red));
                break;
        }

    }

    private void showCaptureStatus(String message, int color) {
        txtStatus.setVisibility(View.VISIBLE);
        txtStatusMessage.setVisibility(View.VISIBLE);
        txtStatusMessage.setText(message.replace("_", " ").toUpperCase());
        txtStatusMessage.setTextColor(color);
    }

    private void loggerSample() {
        ModalLogDetails obj = new ModalLogDetails();
        obj.service_category = "Capture";
        obj.service = "Capture service";

        String loggerUrl = "https://capture.kyc.idfystaging.com/js-logger/publish";
        loggerUrl = "https://webhook.site/1adc0e3f-c10e-4848-ba0f-fbd63df90db2";

        LoggerWrapper loggerWrapper = LoggerWrapper.getInstance();

        // Create instance of logger
        loggerWrapper.createLogger(getActivity(), "capture", token, loggerUrl);
        loggerWrapper.createLogger(getActivity(), "pg", token, loggerUrl);
        loggerWrapper.createLogger(getActivity(), "healthCheck", token, loggerUrl);


        // send logs to server
        //loggerWrapper.getLoggers("capture").log(LogLevel.Info, obj);
        loggerWrapper.logPageRender(System.currentTimeMillis(), "Capture", new HashMap<>());
    }

    public LoggerWrapper getLoggerWrapper() {
        return loggerWrapper;
    }

    private void getPageData() {

        // send logs: after apiCaptureStatusDetails Authenticated
        ModalLogDetails logDetails = new ModalLogDetails();
        logDetails.service_category = "Capture";
        logDetails.service = "ConnectProfileGateway";
        logDetails.event_name = "Connect";
        logDetails.event_type = "SocketState";
        logDetails.event_source = "setupCaptureSocket";
        logDetails.component = "DataService Core";
        logDetails.reference_id = dalCapture.getCaptureId();
        logDetails.reference_type = "AV.TaskID";

        loggerWrapper.log(LogLevel.Info, logDetails, new HashMap<>());


        logDetails = new ModalLogDetails();
        logDetails.service_category = "Capture";
        logDetails.service = "ConnectProfileGateway";
        logDetails.event_name = "Invoked";
        logDetails.event_type = "Initiate";
        logDetails.event_source = "setupVKycSocket";
        logDetails.component = "DataService Core";
        logDetails.reference_id = dalCapture.getCaptureId();
        logDetails.reference_type = "AV.TaskID";

        Map<String, Object> meta = new HashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("t", dalCapture.getToken());
        param.put("capture_id", dalCapture.getCaptureId());
        param.put("session_token", dalCapture.getSessionId());
        meta.put("param", param);

        loggerWrapper.log(LogLevel.Info, logDetails, meta);

        dalCapture.getPageSequence(new ApiCallBack() {
            @Override
            public void onSessionSuccess(JSONObject object) {
                System.out.println("get page data: onSession Success");
                if (object != null) {

                    ModalLogDetails logDetails = new ModalLogDetails();
                    logDetails.service_category = "Capture";
                    logDetails.service = "ConnectProfileGateway";
                    logDetails.event_name = "Session";
                    logDetails.event_type = "ChannelJoined";
                    logDetails.event_source = "setupCaptureSocket";
                    logDetails.component = "DataService Core";
                    logDetails.reference_id = dalCapture.getCaptureId();
                    logDetails.reference_type = "AV.TaskID";

                    loggerWrapper.log(LogLevel.Info, logDetails, new HashMap<>());

                    try {
                        PageSequence pageSequence = dalCapture.getFirstPage();
                        JSONObject payloadObject = null;
                        try {
                            payloadObject = new JSONObject();
                            payloadObject.put("page", pageSequence.getPage());
                            payloadObject.put("payload", new JSONArray());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        fetchPageConfig(pageSequence.getPage(), payloadObject);

                    } catch (Exception e) {
                    }
                } else {
                    if (cTimer != null) {
                        cTimer.cancel();
                        cTimer = null;
                    }
                }
            }

            @Override
            public void intermediateCallBack() {
                requireActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("===RECONNECTing","{}");
                        parentLayout.setVisibility(View.GONE);
                        info_main_layout.setVisibility(View.VISIBLE);
                    }
                });

            }

            @Override
            public void onSessionFailure(String message) {
                //showMessage(message);
                if (message.equals(DALCapture.SOCKET_DISCONNECT)) {
                    disconnectTimer();
                }

            }
        });
    }

    private void disconnectTimer() {
        requireActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                cTimer = new CountDownTimer(30000, 1000) {
                    public void onTick(long millisUntilFinished) {
                        System.out.println("===========> Timer is on ");
                    }

                    public void onFinish() {
                        if (cTimer != null){
                            cTimer.cancel();
                            cTimer = null;
                        }
                        if (currentFragment instanceof CaptureFragment) {
                            dalCapture.disconnectDalSocket();
                            parentLayout.setVisibility(View.GONE);
                            info_main_layout.setVisibility(View.VISIBLE);
                        }
                        // call back to show network disconnected
                    }

                }.start();
            }
        });

    }

    private void renderToolBar(String backgroundColor, String textColor, String logo, String title) {
        ModelToolBar toolBarModel = new ModelToolBar();
        toolBarModel.setBackgroundColor(backgroundColor);
        Toolbar toolbar = components.getToolBar(toolBarModel);
        ModelTextView textViewModel = new ModelTextView();
        textViewModel.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        textViewModel.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        textViewModel.setSize(requireActivity().getResources().getDimensionPixelSize(R.dimen._6ssp));
        textViewModel.setMarginLeft(requireActivity().getResources().getDimensionPixelSize(R.dimen._20sdp));
        textViewModel.setText(title);
        textViewModel.setTextColor(textColor);
        toolbarTitle = components.getTextView(textViewModel);
        ModelImageView imageViewModel = new ModelImageView();
        imageViewModel.setWidth(requireActivity().getResources().getDimensionPixelSize(R.dimen._25sdp));
        imageViewModel.setHeight(requireActivity().getResources().getDimensionPixelSize(R.dimen._25sdp));
        toolbarImage = components.getImageView(imageViewModel);
        toolbarImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requireActivity().onBackPressed();

            }
        });
        Glide.with(getActivity()).load(logo).into(toolbarImage);
        toolbar.addView(toolbarImage);
        toolbar.addView(toolbarTitle);
        headerLinearLayout.addView(toolbar);


    }

    /**
     * This method is used to fetch the page Config
     *
     * @param page
     * @param payloadObject
     */

    public void fetchPageConfig(String page, JSONObject payloadObject) {

        ModalLogDetails logDetails = new ModalLogDetails();
        logDetails.service_category = "Capture";
        logDetails.service = "FetchConfiguration";
        logDetails.event_name = page;
        logDetails.event_type = "Fetch";
        logDetails.event_source = "FetchPageConfigFromBackend";
        logDetails.component = "DataService Core";

        Map<String, Object> metaData = new HashMap<>();
        metaData.put("data", JsonToMap(payloadObject));

        loggerWrapper.log(LogLevel.Info, logDetails, metaData);

        dalCapture.fetchPageConfig("session:fetch_config", payloadObject,
                new ApiCallBack() {
                    @Override
                    public void onSessionSuccess(JSONObject object) {

                        ModalLogDetails logDetails = new ModalLogDetails();
                        logDetails.service_category = "Capture";
                        logDetails.service = "ValidationsCheck";
                        logDetails.event_name = page;
                        logDetails.event_type = "Received";
                        logDetails.event_source = "FetchPageConfigFromBackend";
                        logDetails.component = "DataService Core";

                        metaData.put("inputs", metaData.get("data"));
                        metaData.remove("data");


                        loggerWrapper.log(LogLevel.Info, logDetails, metaData);

                        switch (page) {
                            case "introduction":
                            case "consent": {
                                if (currentFragment instanceof IntroFragment) {
                                    IntroFragment introFragment = (IntroFragment) currentFragment;
                                    introFragment.setPayloadData(object);

                                } else {
                                    loadFragment(IntroFragment.newInstance(object), "IntroFrag");
                                }
                                break;
                            }
                            case "capture":
                                System.out.println("Capture fetch config 1");
                                if (currentFragment instanceof CaptureFragment) {
                                    System.out.println("Capture fetch config 2");
                                    CaptureFragment captureFragment = (CaptureFragment) currentFragment;
                                    captureFragment.setCapturePayload(object);
                                } else {
                                    System.out.println("Capture fetch config 3");
                                    loadFragment(CaptureFragment.newInstance(page), "CaptureFrag");
                                }
                                break;
                            case "permissions":
                                if (currentFragment instanceof PermissionFragment) {
                                    PermissionFragment permissionFragment = (PermissionFragment) currentFragment;
                                    permissionFragment.setJsonPayload(object);
                                    // permissionFragment

                                } else {
                                    loadFragment(PermissionFragment.newInstance(page), "PermissionFrag");
                                }
                                break;
                            case "vkyc.assisted_vkyc":
                                requireActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        AssistedVideoKyc assistedVideoKyc = new AssistedVideoKyc();
                                        assistedVideoKyc.startAvkyc("AssistedVideoKycFrag",object.toString(), childFragMan, R.id.content_layout,BaseFragment.this);
                                    }
                                });
                                break;

                        }

                    }

                    @Override
                    public void intermediateCallBack() {

                    }

                    @Override
                    public void onSessionFailure(String message) {
                        // showMessage(message);

                    }
                });

    }

    public String getSecondaryMainColor() {
        return secondaryMainColor;
    }

    public String getSecondaryContrastColor() {
        return secondaryContrastColor;
    }

    public String getPrimaryMainColor() {
        return primaryMainColor;
    }

    public String getPrimaryContrastColor() {
        return primaryContrastColor;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public String getCaptureId() {
        return captureId;
    }

    public String getSessionId() {
        return session_id;
    }

    public String getToken() {
        return token;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestId() {
        return requestId;
    }

    public String onNextPage() {
        pageIndex++;
        if (pageIndex < listPageSeq.size()) {
            return listPageSeq.get(pageIndex).getPage();
        }
        return "PAGE_END";
    }

    public FragmentManager getChildFragMan() {
        return childFragMan;
    }

    public String getFirstPage() {
        if (listPageSeq.size() != 0) {
            return listPageSeq.get(0).getPage();
        }
        return "";
    }

    /**
     * This Method is used to load the Fragment inside Basefragment
     *
     * @param fragment
     * @param tag
     */
    public void loadFragment(Fragment fragment, String tag) {
        requireActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                currentFragment = fragment;
                FragmentTransaction childFragTrans = childFragMan.beginTransaction();
                childFragTrans.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                childFragTrans.replace(R.id.content_layout, fragment, tag);
                childFragTrans.addToBackStack(tag);
                childFragTrans.commit();
            }
        });

        // requireActivity().getSupportFragmentManager().executePendingTransactions();
    }

    public void setCurrentFragmentName(String tag) {

        Fragment temCurrFrag = childFragMan.findFragmentByTag(tag);
        System.out.println("set current fragment as *********> " + temCurrFrag);
        currentFragment = temCurrFrag;
    }

    /**
     * This method is used to set the Toolbar Title
     *
     * @param title
     */
    public void setToolbarTitle(String title) {
        if (toolbarTitle != null) {
            toolbarTitle.setText(title);
        }
    }

    /**
     * This method is used to set the Toolbar Image
     *
     * @param res
     */
    public void setToolbarImage(int res) {
        if (toolbarImage != null) {
            toolbarImage.setImageResource(res);
        }
    }


    public void sendPageVisitedLog(JSONObject payloadObject) {
        dalCapture.fetchPageConfig("session:page_visit", payloadObject,
                new ApiCallBack() {
                    @Override
                    public void onSessionSuccess(JSONObject object) {
                        Log.d("===PAGEVISITED", object.toString());

                    }

                    @Override
                    public void intermediateCallBack() {

                    }

                    @Override
                    public void onSessionFailure(String message) {
                        // showMessage(message);

                    }
                });

    }

    /**
     * This method is used to hide the header
     */

    public void hideHeader() {
        headerLinearLayout.setVisibility(View.GONE);
    }

    /**
     * This method is used to show the header
     */

    public void showHeader() {
        headerLinearLayout.setVisibility(View.VISIBLE);
    }


    public void showNetworkDisconnected(boolean visiblity) {
        if (visiblity) {
            showHeader();
            parentLayout.setVisibility(View.GONE);
            info_main_layout.setVisibility(View.VISIBLE);
        } else {
            parentLayout.setVisibility(View.VISIBLE);
            info_main_layout.setVisibility(View.GONE);
        }
    }

    public boolean checkFragmentExistsInStack(String tag) {
        Fragment tem = childFragMan.findFragmentByTag(tag);
        if (tem != null) {
            return true;
        }
        return false;
    }

    public boolean checkFragmentIsOnTop(String tag) {

        if (childFragMan.getBackStackEntryCount() > 0) {
            String name = childFragMan.getBackStackEntryAt(childFragMan.getBackStackEntryCount() - 1).getName();
            System.out.println("*****************> " + name);
            if (name == tag) {
                return true;
            }
        }
        return false;
    }

    public String getCurrentFragmentName() {
        System.out.println("*****************> " + currentFragment.getTag());
        return currentFragment.getTag();
    }

    public void popStateCheckFragment() {

        StateCheckFragment stateCheckFragment = new StateCheckFragment();
        childFragMan.beginTransaction().remove(stateCheckFragment);
        childFragMan.beginTransaction().commit();
        //childFragMan.popBackStack();
        childFragMan.popBackStackImmediate();
    }

    public void popPreviousFragment() {
        childFragMan.popBackStackImmediate();
    }


    /**
     * This method is used to clear the backstack
     */

    public void clearBackStack() {
        if (childFragMan != null) {
            for (int i = 0; i < childFragMan.getBackStackEntryCount(); ++i) {
                childFragMan.popBackStack();
            }
        }
    }

    private void multipleSessionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        builder.setTitle(getString(R.string.multiple_sessions));
        builder.setMessage(getString(R.string.active_sessions));
        builder.setCancelable(false);
        builder.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        dalCapture.disconnectDalSocket();
                        getCaptureStatusDetails(true);

                    }
                });

        builder.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onDestroy() {
        loggerWrapper.clearLoggers();
        dalCapture.disconnectDalSocket();
        dalCapture.setCalCompleted(false);
        if (cTimer != null){
            cTimer.cancel();
            cTimer = null;
        }
        super.onDestroy();
    }


    /*@Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            callback = (ReceiveDataLogs)context;
        }catch (Exception e){
            throw new ClassCastException(context.toString()+ " must implement ReceiveDataLogs for receiving data logs");
        }
    }

    private ReceiveDataLogs callback;
    public interface ReceiveDataLogs{
        void receiveLogs(JSONObject object);
    }

    public void sendDataLogs(JSONObject object){
        callback.receiveLogs(object);
    }*/


    @Override
    public void onAvkycSuccess(JSONObject object) {
        switch (object.optString("event")) {
            case "CALL_END":
                clearBackStack();
                PageSequence page = dalCapture.getFirstPage();
                showHeader();
                loadFragment(CaptureFragment.newInstance(page.getPage()), "CaptureFrag");
                break;
        }

    }

    @Override
    public void onAvkycIntermediate(JSONObject object) {
        switch (object.optString("event")) {
            case "TOOLBAR":
                if (object.optBoolean("visibility")) {
                    showHeader();
                    setToolbarTitle(object.optString("title"));
                    setToolbarImage(object.optInt("icon"));
                } else {
                    hideHeader();
                }
                break;
        }
    }

    @Override
    public void onAvkycFailure(JSONObject object) {
        switch (object.optString("event")) {
            case "RECONNECTS_EXHAUSTED":
                showNetworkDisconnected(true);
                clearBackStack();
                PageSequence firstPage = dalCapture.getFirstPage();
                showHeader();
                loadFragment(CaptureFragment.newInstance(firstPage.getPage()), "CaptureFrag");
                break;
            case "NETWORK_DISCONNECTED":
                showNetworkDisconnected(true);
                break;

        }

    }

    CaptureCallBack captureCallBack;

    public void initCallBack(CaptureCallBack captureCallBack) {
        this.captureCallBack = captureCallBack;
    }


    public interface CaptureCallBack {
        void onCaptureSuccess(JSONObject object);
        void onCaptureFailure(JSONObject object);
    }

    private void initFragmentListener() {
        requireActivity().getSupportFragmentManager().setFragmentResultListener(FRAGMENT_REQUEST, getViewLifecycleOwner(), new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                Log.d("==FRAGMENTLSITENER", "LISTENER " + requestKey + " " + result.getString("EVENT"));
                switch (result.getString("EVENT")) {
                    case "HIDE_TOOLBAR":
                        hideHeader();
                        break;
                    case "SHOW_TOOLBAR":
                        showHeader();
                        String image = result.getString("TOOLBAR_IMAGE");
                        if (image.equals("logo")) {
                            setToolbarImage(R.drawable.idfy_main_logo);
                        } else {
                            setToolbarImage(R.drawable.ic_back);
                        }
                        String title = result.getString("TOOLBAR_TITLE");
                        setToolbarTitle(title);
                        break;
                    case "CALL_END":
                        clearBackStack();
                        PageSequence page = dalCapture.getFirstPage();
                        showHeader();
                        loadFragment(CaptureFragment.newInstance(page.getPage()), "CaptureFrag");
                        break;
                    case "NETWORK_DISCONNECTED":
                        showNetworkDisconnected(true);
                        break;
                    case "RECONNECTS_EXHAUSTED":
                        showNetworkDisconnected(true);
                        clearBackStack();
                        PageSequence firstPage = dalCapture.getFirstPage();
                        showHeader();
                        loadFragment(CaptureFragment.newInstance(firstPage.getPage()), "CaptureFrag");
                        break;
                }

            }
        });
    }

    private void getLastLocation() {
        // check if permissions are given
        if (checkPermissions()) {

            // check if location is enabled
            if (isLocationEnabled()) {

                // getting last
                // location from
                // FusedLocationClient
                // object
                if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mFusedLocationClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        Location location = task.getResult();
                        if (location == null) {
                            requestNewLocationData();
                        } else {
                            dalCapture.setLatitude(String.valueOf(location.getLatitude()));
                            dalCapture.setLongitude(String.valueOf(location.getLongitude()));
                            dalCapture.setAccuracy(String.valueOf(location.getAccuracy()));
                            dalCapture.setAltitude(String.valueOf(location.getAltitude()));
                            dalCapture.setSpeed(String.valueOf(location.getSpeed()));
                            getCaptureStatusDetails(false);
                            // latitudeTextView.setText(location.getLatitude() + "");
                            // longitTextView.setText(location.getLongitude() + "");
                        }
                    }
                });
            } else {
                Toast.makeText(getActivity(), "Please turn on" + " your location...", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            //   askForPermission();
        }
    }

    private void requestNewLocationData() {
        LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(5);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
    }

    private LocationCallback mLocationCallback = new LocationCallback() {

        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            Log.d("==LOCATION 2", mLastLocation.getLatitude() + " " + mLastLocation.getLongitude());
            // latitudeTextView.setText("Latitude: " + mLastLocation.getLatitude() + "");
            //longitTextView.setText("Longitude: " + mLastLocation.getLongitude() + "");
        }
    };

    // method to check for permissions
    private boolean checkPermissions() {
        return ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        // If we want background location
        // on Android 10.0 and higher,
        // use:
        // ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED
    }


    // method to check
    // if location is enabled
    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) requireActivity().getSystemService(requireActivity().LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void askForPermission() {
        Dexter.withContext(requireActivity())
                .withPermissions(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            getLastLocation();
                        }
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    private Map<String, Object> JsonToMap(JSONObject obj) {

        Map<String, Object> map = new HashMap<>();
        try {
            Iterator<String> keys = obj.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                map.put(key, obj.get(key));
            }
        } catch (Exception e) {
        }
        return map;
    }

}